.. _using-aia-prep-align-derotate-label:

Using aia_prep.pro to Align and Derotate AIA Data
---------------------------------------------------

In its most basic usage, the ``aia_prep.pro`` routine is called as follows:

.. code-block:: idl

    aia_prep,fnames,indices,out_index,out_data

.. note::

    If it is necessary to revert AIA data to their state prior to the
    despiking process, as discussed in :ref:`despiking-respiking-data-label`,
    and/or to perform a PSF-deconvolution, as discussed in
    :ref:`psf-deconvolution-label`, these operations should be performed
    prior to aia_prep.pro. These operations are not common, but may be necessary
    in some cases.
