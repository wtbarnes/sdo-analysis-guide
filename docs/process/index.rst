.. _how-to-process-label:

How to Process AIA Data
========================

.. toctree::
   :maxdepth: 2

   aia_prep
   despike_respike
   psf
