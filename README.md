# SDO Data Analysis Guide

Source material for the SDO Data Analysis Guide

## Install

First, clone the repository,

```shell
> git clone https://gitlab.com/LMSAL_HUB/aia_hub/sdo-analysis-guide.git
```

and then install the needed dependencies,

```shell
> cd sdo-analysis-guide
> pip install -r requirements.txt
```

## Build

To build the guide in HTML format,

```shell
> make html
```

and open `docs/_build/html/index.html` in your browser. For more info on how to build additional formats, use `make help`.

To ensure that your documentation is properly formatted, run the doc8 linter,

```shell
> doc8 docs
```

## Helpful Links

* [reStructuredText docs](http://docutils.sourceforge.net/rst.html)
* [Sphinx docs](https://www.sphinx-doc.org/en/1.5/index.html)
